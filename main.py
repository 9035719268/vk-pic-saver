import requests
import sys
import time


def get_photos(token, domain, photos_number, offset):
    version = 5.103  # версия VK API
    all_posts = []  # все посты группы

    if photos_number > 100:
        count = 100  # по сколько картинок брать за раз
    else:
        count = photos_number

    while photos_number > 0:
        try:
            response = requests.get('https://api.vk.com/method/wall.get',
                                    params={
                                        'access_token': token,
                                        'v': version,
                                        'domain': domain,
                                        'count': count,
                                        'offset': offset
                                    })
            response.raise_for_status()
        except requests.exceptions.RequestException as err:
            print(err)
            sys.exit(1)

        data = response.json()['response']['items']
        all_posts.extend(data)
        offset += count
        if photos_number >= 100:
            photos_number -= count
            print('Обработано ' + str(count) + ' картинок.')
            print('Осталось обработать ', str(photos_number), ' картинок.')
        else:
            print('Обработано ' + str(photos_number) + ' картинок.')
            photos_number -= photos_number
            print('Все картинки обработаны')

    return all_posts


def save_photos(token, id, owner_id):
    photo_id = id  # id выбранной фотографии
    version = 5.103  # версия VK API

    try:
        response = requests.get('https://api.vk.com/method/photos.copy',
                                params={
                                    'access_token': token,
                                    'owner_id': owner_id,
                                    'photo_id': photo_id,
                                    'v': version
                                })
        response.raise_for_status()
    except requests.exceptions.RequestException as err:
        print(err)
        sys.exit(1)


def handling_photos(token, domain, photos_number, offset, min_likes, owner_id):
    COUNT_PHOTOS = 0  # сколько картинок уже сохранено
    ALL_COUNT_PHOTOS = 0  # сколько картинок будет сохранено
    GROUP_POSTS = get_photos(token, domain, photos_number, offset)  # лист со всеми обработанными картинками

    for post in GROUP_POSTS:
        # Узнать, сколько всего картинок будет сохранено.
        if post['likes']['count'] > int(min_likes) and 'attachments' in post:
            ALL_COUNT_PHOTOS += 1

    for post in GROUP_POSTS:
        # Если лайков на посте больше их требуемого минимального числа и пост - это фотография -> сохранить фотографию
        if post['likes']['count'] > int(min_likes) and 'attachments' in post:
            PHOTO_ID = (post['attachments'][0]['photo']['id'])
            save_photos(token, PHOTO_ID, owner_id)
            COUNT_PHOTOS += 1
            print('Сохранено картинок: ' +
                  str(COUNT_PHOTOS) + ' из ' + str(ALL_COUNT_PHOTOS))
            time.sleep(5)  # ограничение VK API на запросы photos.copy: 1 запрос в 5 секунд
    print("Картинки успешно сохранены в количестве", COUNT_PHOTOS, "штук!")


if __name__ == "__main__":
    __token = input("Введите Ваш токен: ")
    __domain = input("Введите домент группы: ")
    __photos_number = input("Введите количество картинок для обработки: ")
    __offset = input("Введите порядковый номер первой картинки для обработки: ")
    __owner_id = input("Введите id группы: ")
    __min_likes = input("Введите минимальное число лайков на картинке, нужное для ее обработки: ")
    handling_photos(__token, __domain, __photos_number, __offset, __min_likes, __owner_id)
